# Spotify-YouTube Music Downloader

Python 3 script to download songs available on Spotify from YouTube.

## IMPORTANT
This script uses Spotify **only** to collect the information and thus automate the download from YouTube of many songs. The artifice of the download is totally external to Spotify.

**To sum up:** If you want to download 3 albums in particular from an artist, it is enough to give artist name. You need not to search every song from every album in YouTube to download it...

## Information
The script allows you to download playlists, artist (to choose), albums (to choose) and songs.

## Usage
```
python  main.py [-h] [-q] [-t <track uri or track name>, -a <album uri or album name>, -r <artist uri or artist name>, -p <playlist uri or playlsit name>]
```

Where:

```
-t, --track       Spotify track uri or track name
-a, --album       Spotify album uri or album name
-r, --artist      Spotify artist uri or artist name
-p, --playlist    Spotify playlist uri or playlist name
-h, --help        Shows help menu
-q, --quiet       Quiet mode. Doesn't show download status
```

## Requisites
To run the script, firstly you need [Spotify developer account](https://developer.spotify.com/my-applications/#!/login) in order to get **client_id** and **client_secret**. Then export or add to your **.bashrc**:

```
export SPOTIPY_CLIENT_ID='<client_id>'
export SPOTIPY_CLIENT_SECRET='<client_secret>'
```


Also, this libraries have to be installed. You can install with:
```
pip install <library>
```
or
```
pip install --user spotipy tabulate youtube_dl bs4 mutagen
```

- [spotipy](https://github.com/plamere/spotipy)
- [youtube-dl](https://github.com/rg3/youtube-dl)
- [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/)
- [pafy](https://github.com/mps-youtube/pafy)
- [mutagen](https://github.com/quodlibet/mutagen)

And `mp3gain` script for normalize option.

## Examples

### Playlist example

To download 'Global Top 50', just copy URI and run like this:
```
python main.py --playlist spotify:user:spotifycharts:playlist:37i9dQZEVXbMDoHDwVN2tF
```
![image](images/playlist_example.png)

### Artist discography example

To download albums from 'Berri Txarrak' discography:
```
python main.py --artist "Berri Txarrak"
```
Once artists appear, just select them by index.

![image](images/artist_example.png)


### Album example

```
python main.py --album "In waves"
```
Once matched albums appear, select desired one.

![image](images/album_example.png)

### Track example

```
python main.py --track "Itsasoa gara"
```
Once matched tracks appear, select desired one.

![image](images/track_example.png)

## Updates
Updates [here](changelog.md).

## To Do
- Download music without Spotify client. Of course, can't get ID3 tags and it will download songs one by one.
- Save tracks to folder.

## Bugs found
- (1) Downloading playlist by name may not find the playlist. Better using playlist's URI.

## Disclaimer
I am not responsible for the misuse that can be given to the application. Be sensible.
Anyway..., just helps to download songs from YT. Without the script, you probably do the same, but slowly ;).

