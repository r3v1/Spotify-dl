# Changelog
### 2.1 (27/08/2018)
- Normalize tracks option added.
- Python 3 migration. Python 2 is no longer available.

### 2.0 (10/08/2018)
- I have rewritten all the code because I think it was shit in terms of efficiency. Also optimize and simplify code.
- Generate new menus to select the desired artist, album, track or playlist.

### 1.5 (18/09/2017)
- English translate (Everything re-written).
- Control some strings' encoding that can cause troubles.
- Classes separated in different files.

### 1.1 (16/09/2017)
- Volvemos a mostrar el estado de la descarga como lo hacíamos antes.
- Soporte para Python 3.

### 1.0 (07/09/2017)
- Re-implementado todo el código, ahora será Orientado a Objetos. De esta manera, queda más clara la opción de descargar no sólo playlists, sino álbumes a elegir de un artista, un álbum, o una canción sin más.

### 0.08.16 (16/08/2017)
- Corregido el algoritmo de selección de vídeo para que concuerde en duración con el de la playlist de Spotify, así evitar descargar un audio de 5 minutos cuando en la playlist duraba 3, por ejemplo.
- Nueva manera de mostrar por pantalla el estado de la descarga.
