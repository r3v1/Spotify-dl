# !/usr/bin/python2
# -*- coding: utf-8 -*-

import hashlib
import os
from subprocess import check_output, run

import requests
import youtube_dl
from bs4 import BeautifulSoup
from mutagen import MutagenError
from mutagen.id3 import ID3, TIT2, TALB, TPE1, TDRC, APIC, TRCK
from mutagen.id3 import ID3NoHeaderError
from mutagen.mp3 import MP3, HeaderNotFoundError


class FileManager:

    def __init__(self, quiet=True, error=0.05):
        self._ext = '.mp3'
        self._error = error
        ydl_opts = {'format': 'bestaudio/best', 'outtmpl': '%(id)s.%(preferredcodec)s', 'postprocessors': [
            {'key': 'FFmpegExtractAudio', 'preferredcodec': 'mp3', 'preferredquality': '320'}], 'quiet': quiet}
        self._ydl = youtube_dl.YoutubeDL(ydl_opts)

    def _get_youtube_url(self, title, duration):
        """
        Gets the appropriate youtube url of the desired song.
        :param title: title of the song (e.g.: Metallica - One).
        :param duration: duration of the original song.
        :return: url.
        """

        full_link = None
        result = ''
        t = title.replace('%', '%25').replace(' ', '%20').replace('&', '%26').replace('/', '%2F')
        url = "https://www.youtube.com/results?sp=EgIQAQ%253D%253D&q=" + t

        try:
            items = requests.get(url).text
            items_parse = BeautifulSoup(items, "html.parser")

            salir = False
            i = 0

            while not salir:

                result = items_parse.find_all(attrs={'class': 'yt-uix-tile-link'})[i]['href']
                dur_raw = items_parse.find_all(attrs={'class': 'accessible-description'})[i].text.replace('-',
                                                                                                          '').replace(
                    ' ', '').replace('.', '').split(':')

                # To get the video which duration is equal to the original song duration but
                # with an error of +-error rate. Selected video may not be the correct one.
                dur = (int(dur_raw[-2]) * 60 + int(dur_raw[-1])) * 1000
                if abs((float(dur) / float(duration)) - 1) <= self._error:
                    salir = True

                i += 1

            full_link = "youtube.com" + result

        except IndexError:
            '''
           _youtube_url error:

            result = items_parse.find_all(attrs={'class': 'yt-uix-tile-link'})[0]['href']
                IndexError: list index out of range
            '''

            print('\033[1;31m[!]\033[0m Can\'t find the song. Increasing error rate...')
            self._error += 0.05
            full_link = self._get_youtube_url(title, duration)
            self._error = 0.05


        except Exception:
            exit(1)

        return full_link

    def _download_song(self, link, title):
        """
        Downloads the song with pafy library.
        :param link: youtube link.
        :return: file name.
        """

        h = hashlib.sha1(str(link).encode('utf-8')).hexdigest()
        tmp = (title + self._ext).encode('utf-8')

        if not (os.path.exists(h) or os.path.exists(h + ".m4a") or os.path.exists(h + self._ext) or os.path.exists(
                tmp)):
            print("\n\033[1;33m[*]\033[0m Downloading '%s'..." % title)
            try:
                r = self._ydl.extract_info("http://www." + link, download=True)
                song_file = r['id'] + self._ext
                os.rename(song_file, h)
            except youtube_dl.utils.DownloadError:
                return self._download_song(link, title)

        else:
            print("\n\033[1;32m[+]\033[0m Downloaded '%s'..." % title)
            if os.path.exists(h + ".m4a"):
                h = h + ".m4a"
            elif os.path.exists(h + self._ext):
                h = h + self._ext
            elif os.path.exists(title + self._ext):
                h = title + self._ext

        if not h:
            print('\033[1;31m[!]\033[0m Download cancelled!')

        return h

    def _convert_to_mp3(self, file_to_convert):
        try:
            os.system("ffmpeg -i %s -ab 320k %s.mp3" % (
                file_to_convert, file_to_convert.replace(file_to_convert.split('.')[-1], '')))
        except Exception:
            return None

        return file_to_convert.replace(file_to_convert.split('.')[-1], '') + self._ext

    @staticmethod
    def _download_album_art(link):
        """
        Downloads song's album art.
        :param link: Album art's link.
        """

        h = hashlib.sha1(str(link).encode('utf-8')).hexdigest()

        if not os.path.exists(h + '.jpg'):
            try:
                raw = requests.get(link).content
                cover = open(h + '.jpg', 'wb')
                cover.write(raw)
                cover.close()
            except requests.exceptions.MissingSchema as e:
                print('\n\033[1;31m[!]\033[0m Can\'t download album art: %s.\n' % e)
            except Exception as e:
                print('\n\033[1;31m[!]\033[0m URLLIB ERROR: %s.\n' % e)

        return h + '.jpg'

    def _set_meta_tags(self, music_file, meta_tags):
        """
        Set's ID3 tags to the downloaded MP3 file.
        :param music_file: path to the song.
        :param i: position of the song in meta_tags array.
        :param array_number:
        """
        try:
            can = ID3(music_file)
        except ID3NoHeaderError:
            can = ID3()
        except MutagenError:
            music_file = music_file.replace('m4a', self._ext)
            can = ID3(music_file)

        print('\033[1;33m[*]\033[0m Setting metatags...')

        # Track number
        can["TRCK"] = TRCK(encoding=3, text=str(meta_tags['track_number']))
        # title
        can["TIT2"] = TIT2(encoding=3, text=meta_tags['track_name'])
        # Album
        can["TALB"] = TALB(encoding=3, text=meta_tags['album_name'])
        # Artist
        can["TPE1"] = TPE1(encoding=3, text=meta_tags['artist'])
        # Year
        can["TDRC"] = TDRC(encoding=3, text=str(meta_tags['album_date']))
        # Genre
        # can['TCON'] = TCON(encoding=3, text=meta_tags[''])

        can.save(music_file)

        cover = self._download_album_art(meta_tags['cover'])
        try:
            audio = MP3(music_file, ID3=ID3)
            audio.tags.add(
                APIC(
                    encoding=3,  # 3 is for utf-8
                    mime='image/jpg',  # image/jpeg or image/png
                    type=3,  # 3 is for the cover image
                    desc=u'Cover',
                    data=open(cover, 'rb').read()
                )
            )
            audio.save()
        except IOError as e:
            print('\n\033[1;31m[!]\033[0m IO Error: %s.' % e)
        except HeaderNotFoundError as e:
            print('\n\033[1;31m[!]\033[0m Header not found error: %s.' % e)

    def download(self, track_metadata, download_info=""):
        title = track_metadata['track_name']
        artist = track_metadata['artist']
        duration = track_metadata['duration']

        if len(download_info) > 0:
            print(download_info)

        l = self._get_youtube_url("%s - %s" % (title, artist), duration)
        f = self._download_song(l, title)
        if f.find('.m4a') >= 0:
            f = self._convert_to_mp3(f)
        if f:
            self._set_meta_tags(f, track_metadata)
            os.rename(f, title + self._ext)

            print('\033[1;32m[+]\033[0m Done!\n')

    @staticmethod
    def remove_pictures():
        try:
            filelist = [f for f in os.listdir(os.getcwd()) if f.endswith(".jpg")]
            for f in filelist:
                os.remove(os.path.join(os.getcwd(), f))
        except Exception:
            pass

    @staticmethod
    def normalize():
        """
        Normalize downloaded songs.
        :return:
        """
        max_gain = 210
        files = os.listdir(os.path.curdir)
        files = [x for x in files if x.find('mp3') > -1]

        for mp3 in files:

            command = 'mp3gain "{}" | grep "Max mp3" | rev | cut -d " " -f 1 | rev'.format(mp3)
            output = check_output(['bash', '-c', command])
            try:
                gain = int(output.strip())

                if gain != max_gain:
                    # Normalize
                    g = max_gain - gain
                    print('\n\033[1;32m[+]\033[0m Applying gain change of {0} to {1}\n'.format(g, mp3))
                    command = 'mp3gain -g {0} {1}'.format(g, mp3)
                    run(['bash', '-c', command])

            except Exception as e:
                print(e)
