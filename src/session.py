# !/usr/bin/python
# -*- coding: utf-8 -*-


import os

import menu
import spotipy
import spotipy.oauth2 as oauth
from tabulate import tabulate


class SpotifyClient:

    def __init__(self):
        self._session = spotipy.Spotify
        self._logged_in = False
        self._search_limit = 15

    @staticmethod
    def _get_client():
        try:
            return os.environ['SPOTIPY_CLIENT_ID'], os.environ['SPOTIPY_CLIENT_SECRET']
        except Exception as e:
            print(e)
            print("[!] CLIENT_ID or CLIENT_SECRET token not found!")
            print("\n\tAdd them to your .bashrc:")
            print("\texport SPOTIPY_CLIENT_ID=<client_id>")
            print("\texport SPOTIPY_CLIENT_SECRET=<client_secret>")
            exit(1)

    def get_session(self):
        if not self._logged_in:
            try:
                oauth2 = oauth.SpotifyClientCredentials(client_id=self._get_client()[0],
                                                        client_secret=self._get_client()[1])
                token = oauth2.get_access_token()
                self._session = spotipy.Spotify(auth=token)
                self._logged_in = True
            except oauth.SpotifyOauthError:
                print('\n\033[1;31m[!]\033[0m Authentication error. Try again later.\n')
                exit(1)

            except Exception as e:
                print('\n\033[1;31m[!]\033[0m Can\'t log in: %s\n' % e)
                exit(1)

        return self._session

    def search_artist_uri(self, artist):
        items = self._session.search(q='artist:' + artist, type='artist', limit=self._search_limit)['artists']['items']
        if len(items) > 0:
            print("\n[*] Many artists found:\n")
            artists = []
            i = 1
            for t in items:
                artists.append(
                    [i, t['name'], t['popularity']])
                i += 1

            print("")
            print(tabulate(artists, headers=["#", "Artist", "Popularity"]))

            selection = menu.make_selection(maximum=len(items), multiple_selection=False)

            return items[selection - 1]['uri']
        else:
            return None

    def search_album_uri(self, album):
        items = self._session.search(q='album:' + album, type='album', limit=self._search_limit)['albums']['items']
        if len(items) > 0:
            print("\n[*] Many albums found:\n")
            albums = []
            i = 1
            for t in items:
                albums.append(
                    [i, t['name'], t['artists'][0]['name'], t['release_date']])
                i += 1

            print("")
            print(tabulate(albums, headers=["#", "Album", "Artist", "Date"]))

            selection = menu.make_selection(maximum=len(items), multiple_selection=False)

            return items[selection - 1]['uri']
        else:
            return None

    def search_track_uri(self, track):
        items = self._session.search(q='track:' + track, type='track', limit=self._search_limit)['tracks']['items']
        if len(items) > 0:
            print("\n[*] Many tracks found:\n")
            tracks = []
            i = 1
            for t in items:
                m, s = menu.duration_to_mins(t['duration_ms'])
                duration = str("%sm %ss" % (m, s))
                tracks.append(
                    [i, t['name'], t['artists'][0]['name'], t['album']['name'], t['album']['release_date'], duration])
                i += 1

            print("")
            print(tabulate(tracks, headers=["#", "Title", "Artist", "Album", "Date", "Duration"]))

            selection = menu.make_selection(maximum=len(items), multiple_selection=False)

            return items[selection - 1]['uri']
        else:
            return None

    def search_playlist_uri(self, playlist):
        items = self._session.search(q='playlist:' + playlist, type='playlist', limit=self._search_limit)['playlists'][
            'items']
        if len(items) > 0:
            print("\n[*] Many playlists found:\n")
            playlists = []
            i = 1
            for t in items:
                playlists.append(
                    [i, t['name']])
                i += 1

            print("")
            print(tabulate(playlists, headers=["#", "Playlist"]))

            selection = menu.make_selection(maximum=len(items), multiple_selection=False)

            return items[selection - 1]['uri']
        else:
            return None

    def get_artist_albums(self, artist_uri):
        return self._session.artist_albums(artist_uri, album_type='album')

    def get_playlist(self, playlist_uri):
        if playlist_uri:
            user = playlist_uri.split(':')[2]
            uri = playlist_uri.split(':')[-1]

            return self._session.user_playlist_tracks(user, playlist_id=uri)
        return None

    def get_album(self, album_uri):
        if album_uri:
            return self._session.album(album_uri)
        return None

    def get_track(self, track_uri):
        if track_uri:
            return self._session.track(track_uri)
        return None
