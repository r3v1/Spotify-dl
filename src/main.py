# !/usr/bin/python
# -*- coding: utf-8 -*-

import getopt
import sys

from menu import Menu


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'hqt:a:r:p:n',
                                   ['help', 'quiet', 'track=', 'album=', 'artist=', 'playlist=', 'normalize'])

    except getopt.GetoptError:
        usage()
        sys.exit()

    # Valores predeterminados
    quiet = False
    tipo = None
    search = None
    normalize = False

    if len(opts) > 0:

        for opt, arg in opts:
            if opt in ('-h', '--help'):
                usage()
                sys.exit()

            elif opt in ('-q', '--quiet'):
                quiet = True

            elif opt in ('-t', '--track'):
                tipo = 'track'
                search = arg
            elif opt in ('-a', '--album'):
                tipo = 'album'
                search = arg
            elif opt in ('-r', '--artist'):
                tipo = 'artist'
                search = arg
            elif opt in ('-p', '--playlist'):
                tipo = 'playlist'
                search = arg

            elif opt in ('-n', '--normalize'):
                normalize = True

            else:
                usage()
                sys.exit()

        m = Menu(quiet=quiet)
        m.menu(tipo, search, normalize=normalize)

    else:
        usage()
        sys.exit()


def usage():
    print(
        "usage:\tmain.py [OPTIONS] [-t <track uri or track name>, -a <album uri or album name>, -r <artist uri or "
        "artist name>, -p <playlist uri or playlsit name>]")

    print("\nUsage:")
    print("\t-t, --track\t  Spotify track uri or track name")
    print("\t-a, --album\t  Spotify album uri or album name")
    print("\t-r, --artist\t  Spotify artist uri or artist name")
    print("\t-p, --playlist\t  Spotify playlist uri or playlist name")

    print("\nOptions:")
    print("\t-h, --help\t  Shows this help")
    print("\t-q, --quiet\t  Quiet mode")
    print("\t-n, --normalize\t  Normalize downloaded songs")


if __name__ == "__main__":
    main(sys.argv[1:])
