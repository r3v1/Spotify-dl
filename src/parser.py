# !/usr/bin/python
# -*- coding: utf-8 -*-


def get_track_metadata_from_album(raw_album):
    if raw_album:
        metadata = []

        for track in raw_album['tracks']['items']:
            track_name = track['name']
            track_uri = track['uri']
            track_number = track['track_number']
            disc_number = track['disc_number']
            duration = track['duration_ms']
            artist = track['artists'][0]['name']
            album_name = raw_album['name']
            cover = raw_album['images'][0]['url']
            album_uri = raw_album['uri']
            album_date = raw_album['release_date'].split('-')[0]
            try:
                genre = raw_album['genres'][0]
            except Exception:
                genre = ''

            metadata.append({"track_name": track_name, "duration": duration, "disc_number": disc_number,
                             "track_uri": track_uri, "track_number": track_number, "artist": artist,
                             "album_name": album_name, "genre": genre, "cover": cover, "album_uri": album_uri,
                             "album_date": album_date})

        return metadata
    else:
        return None


def get_album_metadata_from(raw_album):
    if raw_album:
        album_name = raw_album['name']
        album_artist = raw_album['artists'][0]['name']
        image_url = raw_album['images'][0]['url']
        album_uri = raw_album['uri']
        album_date = raw_album['release_date'].split('-')[0]
        try:
            genre = raw_album['genres'][0]
        except Exception:
            genre = ''

        return {"artist": album_artist, "album_name": album_name, "image_url": image_url, "album_uri": album_uri,
                "album_date": album_date, "genre": genre}
    else:
        return None


def get_artist_albums_metadata_from(raw_artist_albums):
    if raw_artist_albums:
        # albums = spotify_session.artist_album(raw_artist['uri'], album_type='album')

        albums_metadata = []
        for album in raw_artist_albums['items']:
            album_name = album['name']
            album_uri = album['uri']
            album_artist = album['artists'][0]['name']
            album_date = album['release_date'].split('-')[0]

            albums_metadata.append(
                {"album_name": album_name, "album_uri": album_uri, "album_artist": album_artist, "album_date": album_date})

        return albums_metadata
    else:
        return None


def get_track_metadata_from(raw_song):
    if raw_song:
        track_name = raw_song['name']
        album_name = raw_song['album']['name']
        album_date = raw_song['album']['release_date'].split('-')[0]
        album_uri = raw_song['album']['uri']
        artist = raw_song['album']['artists'][0]['name']
        cover = raw_song['album']['images'][0]['url']
        duration = raw_song['duration_ms']
        track_number = raw_song['track_number']
        track_uri = raw_song['uri']

        return {"track_name": track_name, "album_name": album_name, "album_date": album_date, "album_uri": album_uri,
                "artist": artist, "cover": cover, "duration": duration, "track_uri": track_uri,
                "track_number": track_number}
    else:
        return None


def get_playlist_tracks_from(raw_playlist):
    if raw_playlist:
        tracks = []
        for item in raw_playlist['items']:
            tracks.append(
                get_track_metadata_from(item['track'])
            )

        return tracks
    else:
        return None
