# !/usr/bin/python
# -*- coding: utf-8 -*-

import parser
import re
import sys

import session
from file import FileManager
from tabulate import tabulate


class Menu:
    def __init__(self, quiet=True):
        self.client = session.SpotifyClient()
        self.client.get_session()
        self.fm = FileManager(quiet=quiet)

    @staticmethod
    def _is_uri(search):
        """
            Checks if the given search is uri or not.

            :param search:
            :return: boolean
            """
        if re.match('spotify\:(track|album|user|artist)\:([a-z0-9]*\:playlist\:[0-z]*|[0-z]*)', search, re.M | re.I):
            # print("[*] URI found.")
            return True
        else:
            # print("[*] Name found.")
            return False

    def grab_track(self, search):
        if search:
            if self._is_uri(search):
                track_info = self.client.get_track(search)
            else:
                track_info = self.client.get_track(self.client.search_track_uri(search))

            return parser.get_track_metadata_from(track_info)
        else:
            return None

    def grab_album(self, search):
        if search:
            if self._is_uri(search):
                album_info = self.client.get_album(search)
            else:
                album_info = self.client.get_album(self.client.search_album_uri(search))

            metadata = parser.get_track_metadata_from_album(album_info)

            return metadata

        else:
            return None

    def grab_artist(self, search):
        if search:
            if self._is_uri(search):
                artist_info = self.client.get_artist_albums(search)
            else:
                artist_info = self.client.get_artist_albums(self.client.search_artist_uri(search))

            metadata = parser.get_artist_albums_metadata_from(artist_info)

            return metadata
        else:
            return None

    def grab_playlist(self, search):
        if search:
            if self._is_uri(search):
                playlist_info = self.client.get_playlist(search)
            else:
                playlist_info = self.client.get_playlist(self.client.search_playlist_uri(search))

            tracks = parser.get_playlist_tracks_from(playlist_info)

            return tracks
        else:
            return None

    @staticmethod
    def show_song(raw):
        m, s = duration_to_mins(raw['duration'])
        duration = str("%sm %ss" % (m, s))
        print("")
        print(tabulate(
            [[raw['track_number'], raw['track_name'], raw['album_name'], raw['artist'], raw['album_date'], duration]],
            headers=["Track", "Title", "Album", "Artist", "Date", "Duration"]))

    @staticmethod
    def show_album(raw_dict):
        tracks = []
        for t in raw_dict:
            m, s = duration_to_mins(t['duration'])
            duration = str("%sm %ss" % (m, s))
            tracks.append(
                [t['track_number'], t['track_name'], t['artist'], t['album_name'], t['album_date'], duration])

        print("")
        print(tabulate(tracks, headers=["#", "Title", "Artist", "Album", "Date", "Duration"]))

    @staticmethod
    def show_artist_albums_metadata(raw):
        info = []
        i = 1
        for album in raw:
            info.append([i, album['album_name'], album['album_artist'], album['album_date']])
            i += 1
        print("")
        print(tabulate(info, headers=['#', 'Album', 'Artist', 'Date']))

    @staticmethod
    def show_playlist_tracks(raw):
        tracks = []
        i = 1
        for track in raw:
            m, s = duration_to_mins(track['duration'])
            duration = str("%sm %ss" % (m, s))
            tracks.append([i, track['track_name'], track['album_name'], track['artist'], track['album_date'], duration])
            i += 1
        print("")
        print(tabulate(tracks, headers=['#', 'Name', 'Album', 'Artist', 'Date', 'Duration']))

    def menu(self, t, search, normalize=False):
        try:
            if t == 'track':
                self._track_menu(search=search, force_download=False)

            elif t == 'album':
                self._album_menu(search=search)

            elif t == 'artist':
                self._artist_menu(search=search)

            elif t == 'playlist':
                self._playlist_menu(search=search)

            if normalize:
                self.fm.normalize()

        except KeyboardInterrupt:
            print("\n--> Ctrl+c reached. Exiting!")

        self.fm.remove_pictures()

    def _playlist_menu(self, search):
        info = self.grab_playlist(search)
        if info:
            self.show_playlist_tracks(info)
            selection = make_selection(maximum=len(info))
            for s in selection:
                d_info = str("[Track %s of %s]" % (selection.index(s) + 1, len(selection)), )
                track_uri = info[s - 1]['track_uri']
                self._track_menu(track_uri, download_info=d_info, force_download=True)
        else:
            print("\n[!] Playlist not found.")

    def _artist_menu(self, search):
        info = self.grab_artist(search)
        if info:
            self.show_artist_albums_metadata(info)
            selection = make_selection(maximum=len(info))
            for s in selection:
                d_info = str("[Album %s of %s]" % (selection.index(s) + 1, len(selection)) + "\n")
                album_uri = info[s - 1]['album_uri']
                self._album_menu(album_uri, download_info=d_info)
        else:
            print("\n[!] Artist not found.")

    def _album_menu(self, search, download_info=""):
        info = self.grab_album(search)
        if info:
            self.show_album(info)
            selection = make_selection(maximum=len(info))

            for s in selection:
                d_info = download_info + str("[Track %s of %s]" % (selection.index(s) + 1, len(selection)), )
                track_uri = info[s - 1]['track_uri']
                # self.fm.download(info[s - 1], download_info=d_info)
                self._track_menu(track_uri, force_download=True, download_info=d_info)
        else:
            print("\n[!] Album not found.")

    def _track_menu(self, search, force_download=False, download_info=""):
        info = self.grab_track(search)

        if info:
            if not force_download:
                self.show_song(info)
                o = str(input("\n===> Download '%s'? [Y/n]: " % info['track_name']))
            else:
                o = 'Y'

            if o.lower().find('n') or len(o) == 0:
                self.fm.download(info, download_info=download_info)
        else:
            print("\n[!] Track not found.")


def duration_to_mins(raw_duration):
    try:
        d = float(raw_duration) / 1000.0
        mins = str(int(d / 60))
        secs = str(int(d % 60))

    except ValueError:
        mins = '?'
        secs = '?'

    return mins, secs


def make_selection(maximum=40, multiple_selection=True):
    if multiple_selection:
        msg = "\n===> Select items to download [1, 5, 6-9,...] (0 = all): "
    else:
        msg = "\n===> Select item to download: "

    salir = False

    while not salir:
        selection = str(input(msg)).replace(' ', '').split(
            ',')
        final = []
        i = 0
        for s in selection:
            try:
                if s.find('-') > -1:
                    first = int(s.split('-')[0])
                    last = int(s.split('-')[-1])

                    for item in range(first, last + 1):
                        if 1 <= item <= maximum:
                            final.append(item)
                        i += 1
                    salir = True
                elif s == '0':
                    for i in range(1, maximum + 1):
                        final.append(i)
                    salir = True
                else:
                    if 1 <= int(s) <= maximum:
                        final.append(int(s))
                        salir = True
                    else:
                        salir = False

            except TypeError:
                print("[!] No item selected. Exiting!")
                sys.exit(1)
            except ValueError:
                print("[!] No item selected. Exiting!")
                sys.exit(1)
            except IndexError:
                salir = False

    if multiple_selection:
        return sorted(set(final))
    else:
        return sorted(set(final))[0]


if __name__ == "__main__":
    m = Menu(quiet=False)

    # Song example
    m.menu('track', 'Mi Casa reno', normalize=True)

    # Album example
    # m.menu('album', "The Holographic Principle")
    # m.menu('album', "spotify:album:0vmTgLDR0jaxFt6VuiS9Ut")

    # # Artist example
    # m.menu('artist', "spotify:artist:4KpgeSLtjj0txr6drzaedu")
    # m.menu('artist', "talco")

    # # Playlist example
    # m.menu('playlist', 'spotify:user:11136258914:playlist:5GdwOCQhpYtBKGFhadpRtS')
    # m.menu('playlist', 'One Love')
    # m.show_playlist_tracks(tracks)
